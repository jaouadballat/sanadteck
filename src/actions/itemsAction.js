import * as ActionType from './actionTypes';

export function addItem() {
    return {
        type: ActionType.ADD_ITEM,
        payload: {
            Name: '',
            Type: '',
            Value: ''
        }
    }
}

export function deleteItem(payload) {
    return {
        type: ActionType.DELETE_ITEM,
        payload
    }
}
