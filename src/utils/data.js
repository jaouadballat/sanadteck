export const initialState = {
    dataTree: [
        {
            id: '1',
            Name: '',
            Type: '',
            Value: '',
            children: [
                {
                    id: '1-1',
                    Name: '',
                    Type: '',
                    Value: '',
                    children: [
                        {
                            id: '1-1-1',
                            Name: '',
                            Type: '',
                            Value: ''
                        },
                        {
                            id: '1-1-2',
                            Name: '',
                            Type: '',
                            Value: ''
                        }
                    ]
                }
            ]
        },
        {
            id: '2',
            Name: '',
            Type: '',
            Value: '',
        },
        {
            id: '3',
            Name: '',
            Type: '',
            Value: '',
            children: [
                {
                    id: '3-1',
                    Name: '',
                    Type: '',
                    Value: ''
                },
                {
                    id: '3-2',
                    Name: '',
                    Type: '',
                    Value: ''
                }
            ]
        }
    ]
}

