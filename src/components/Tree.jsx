import React, { Component } from 'react';
import { Collection, CollectionItem, Button, Row, Input } from 'react-materialize';

export default class Tree extends Component {
  render() {
      console.log(this.props)

      const options = ['Boolean', 'Array', 'Object', 'Number'].map((option, index) => (
          <option key={index} value={option}>{option}</option>
      ));
      return (
          <div className="container">
              <div className="row">
                  <div className="col s12">
                      <Collection className="visible">
                          <CollectionItem>
                              <Row>
                                  <Input label="Name" s={6} />
                                  <Input type='select'>
                                      <option  value="">Type</option>
                                      {options}
                                  </Input>
                                  <Input label="Value" />
                                  <Button className='red delete_button' onClick={() => this.props.onClick(this.props.data)}>
                                      <i className="material-icons">add</i>
                                  </Button>
                              </Row>
                          </CollectionItem>
                      </Collection>
                  </div>
              </div>
          </div>
      )
  }
}

