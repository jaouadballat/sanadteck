import React, { Component } from 'react';
import { Collection, CollectionItem, Button, Row, Input } from 'react-materialize';
import { connect } from 'react-redux';

import { deleteItem } from '../actions/itemsAction';


class Tre extends Component {
    constructor(props) {
        super(props);
        this.state = {
            chevron: 'chevron_right',
            toggle: false,
            childNumber: 0
        }
    }

    onChangeSelect = (e, data) => {
        console.dir(e.target);
    }

    toggleButton = () => {
 
        const chevron = this.state.chevron === 'chevron_right' ? 'expand_more' : 'chevron_right';
        this.setState({chevron, toggle: !this.state.toggle});

    }

    deleteItem(index, data) {
        //this.props.deleteItem(index)
        console.log(data)
        console.log(this.props)
    }


    render() {
        const { dataTree } = this.props;

        const options = ['Boolean', 'Array', 'Object', 'Number'].map((option, index) => (
            <option key={index} value={option}>{option}</option>
        ));

        return (
            <div>
                {
                    dataTree.map((data, index) => (
                        <div className="col s12" key={index}>
                            <Collection className="visible">
                                <CollectionItem href='javascript:void(0)'>
                                    <Row>
                                        {data.children &&
                                            <Button floating className='blue toggle_button'
                                                onClick={this.toggleButton}
                                            >
                                            <i className="material-icons">{this.state.chevron}</i>
                                            </Button>
                                        }
                                        <Input label="Name" s={6} />
                                        <Input type='select' onChange={(e) => this.onChangeSelect(e, data)}>
                                            <option value="">Type</option>
                                            {options}
                                        </Input>
                                        <Input label="Value" />
                                        <Button className='red delete_button'
                                         onClick={() => this.deleteItem(index, data)}>
                                            <i className="material-icons">delete_outline</i>
                                        </Button>
                                    </Row>
                                </CollectionItem>
                            </Collection>
                            {data.children && this.state.toggle && <Tre dataTree={data.children} />}
                        </div>
                    ))
                }
            </div>
        );
    }
}

export default connect(null, { deleteItem })(Tre);
