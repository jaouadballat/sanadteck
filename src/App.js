import React, { Component } from 'react';
import { Button } from 'react-materialize';
import { connect } from 'react-redux';
import { addItem } from './actions/itemsAction';
import './App.css';
import Tre from './components/Tre';


class App extends Component {

  constructor(props) {
    super(props);
  }

   addItem = () => {
      this.props.addItem();
   }



  render() {
    return (
      <div>
        <Button floating className='blue' large style={{ top: '45px', left: '24px' }} onClick={this.addItem} >
          <i className="material-icons">add</i>
        </Button>
        <div className="container">
          <div className="row">
            <Tre dataTree={this.props.dataTree} />
          </div>
        </div>

      </div>
    );
  }
}

function mapStateToProps(state) {
    return {
        dataTree: state.itemsReducer.dataTree
    }
}


export default connect(mapStateToProps, { addItem })(App);
