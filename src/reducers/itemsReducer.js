import * as ActionType from '../actions/actionTypes';

import { initialState } from '../utils/data2';


export default function (state=initialState, action) {
    switch(action.type) {
        case ActionType.ADD_ITEM: 
           return {
                ...state,
                dataTree: [...state.dataTree, action.payload]
           }

        case ActionType.DELETE_ITEM: 
           
           return {
               ...state,
               dataTree: state.dataTree.splice(0, action.payload)
           }

        default: 
            return state
    }
}