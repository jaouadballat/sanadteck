import { combineReducers } from 'redux';
import itemsReducer from './itemsReducer';

export  const reducers = combineReducers({
    itemsReducer
});